<?php

use Muyuym\Tools\Sql;

class SqlTest extends TestCase
{
    public function testStrFromMap()
    {
        $map = [
            'name' => 'hai',
            'age' => '99'
        ];
        $sql = Sql::sql_from_map('user', $map);
        $this->assertSame("INSERT INTO `user` (`name`, `age`) VALUES ('hai', '99');", $sql);
        $sql = Sql::sql_from_map('user', $map, Sql::UPDATE, 'id = 1');
        $this->assertSame("UPDATE `user` SET `name` = 'hai', `age` = '99' WHERE id = 1;", $sql);
    }
}
