<?php

namespace Muyuym\Tools\Structs;

class ListSearch extends Base
{
    /**
     * @var bool 是否分页查询
     */
    public bool $paging = true;

    public int $perPage = 10;

    public int $pageSize = 10;
}
