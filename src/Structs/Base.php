<?php

namespace Muyuym\Tools\Structs;

use Muyuym\Tools\Helpers\Str;
use ReflectionClass;

abstract class Base
{
    public function __construct($data = [])
    {
        $ref = new ReflectionClass($this);
        foreach ($ref->getProperties() as $property) {
            // $data 中的属性key
            $property_key = $property->getName();
            if (!isset($data[$property_key])) {
                $property_key = Str::camel($property->getName());
            }
            if (!isset($data[$property_key])) {
                $property_key = Str::snake($property->getName());
            }
            if (!isset($data[$property_key])) {
                continue;
            }

            if ($property->getType() instanceof \ReflectionNamedType) {
                if (enum_exists($property->getType()->getName())) {
                    $this->{$property->getName()} = $this->handleEnum($property, $data[$property_key]);
                    continue;
                }

                if (class_exists($property->getType()->getName())) {
                    $class_name = $property->getType()->getName();
                    $this->{$property->getName()} = new $class_name($data[$property_key]);
                    continue;
                }

                if (is_scalar($data[$property_key])) {
                    settype($data[$property_key], $property->getType()->getName());
                    $this->{$property->getName()} = $data[$property_key];
                    continue;
                }
            }

            $defined_type_vec = [];
            if ($property->getType() instanceof \ReflectionNamedType) {
                $defined_type_vec[] = $property->getType()->getName();
            } elseif ($property->getType() instanceof \ReflectionUnionType) {
                foreach ($property->getType()->getTypes() as $type) {
                    $defined_type_vec[] = $type->getName();
                }
            }

            if ($this->matchType($defined_type_vec, gettype($data[$property_key]))) {
                $this->{$property->getName()} = $data[$property_key];
            }
        }
    }

    private function handleEnum(\ReflectionProperty $property, $value)
    {
        $type = $property->getType()->getName();
        if ($value instanceof $type) {
            return $value;
        } else {
            $ref = new \ReflectionEnum($type);
            if ($ref->isBacked()) {
                return $type::tryFrom($value);
            } else {
                $name = null;
                if ($ref->hasCase($value)) {
                    $name = $value;
                } elseif ($ref->hasCase(ucfirst($value))) {
                    $name = ucfirst($value);
                }
                return $name ? $ref->getCase($name)->getValue() : null;
            }
        }
    }

    protected function matchType($defined_type_vec, $value_type): bool
    {
        if (in_array('int', $defined_type_vec)) {
            $defined_type_vec[] = 'integer';
        }
        if (in_array('float', $defined_type_vec)) {
            $defined_type_vec[] = 'double';
        }
        if (in_array('bool', $defined_type_vec)) {
            $defined_type_vec[] = 'boolean';
        }
        return in_array($value_type, $defined_type_vec);
    }
}
