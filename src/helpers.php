<?php

if (!function_exists('pp')) {
    // pretty print
    function pp($var, $dump = false): void
    {
        $is_cli = PHP_SAPI == 'cli';
        if (!$is_cli) {
            echo '<pre>';
        }
        $dump ? var_dump($var) : print_r($var);
        if ($is_cli) {
            if (!(is_array($var) || is_object($var))) {
                echo "\n";
            }
        } else {
            echo '</pre>';
        }
    }
}

if (!function_exists('res_ok')) {
    function res_ok($message = 'ok', $data = []): \Muyuym\Tools\Structs\Result
    {
        $result = new \Muyuym\Tools\Structs\Result();
        $result->code = 0;
        $result->message = $message;
        $result->data = $data;
        return $result;
    }
}

if (! function_exists('res_err')) {
    function res_err($message = 'err', $code = 1, $data = []): \Muyuym\Tools\Structs\Result
    {
        $result = new \Muyuym\Tools\Structs\Result();
        $result->code = $code;
        $result->message = $message;
        $result->data = $data;
        return $result;
    }
}

if (! function_exists('res_from')) {
    function res_from(\GrahamCampbell\ResultType\Result|\PhpOption\Option $res): \Muyuym\Tools\Structs\Result
    {
        if ($res instanceof \GrahamCampbell\ResultType\Result) {
            if ($res->success()->isEmpty()) {
                $e = $res->error()->get();
                if (is_scalar($e)) {
                    return res_err($e);
                } else {
                    return res_err('err', 1, $e);
                }
            } else {
                $data = [];
                $val = $res->success()->get();
                if (is_array($val) || is_object($val)) {
                    $data = $val;
                } else {
                    $data[] = $val;
                }
                return res_ok('ok', $data);
            }
        }
        if ($res instanceof \PhpOption\Option) {
            if ($res->isEmpty()) {
                return res_err();
            } else {
                $data = [];
                $val = $res->get();
                if (is_array($val) || is_object($val)) {
                    $data = $val;
                } else {
                    $data[] = $val;
                }
                return res_ok('ok', $data);
            }
        }
        return res_err();
    }
}

if (!function_exists('isset_or')) {
    function isset_or($needle, $haystack, $default = null) {
        return $haystack[$needle] ?? $default;
    }
}

if (!function_exists('isset_or_str')) {
    function isset_or_str($needle, $haystack) {
        return isset_or($needle, $haystack, '');
    }
}

if (!function_exists('isset_or_int')) {
    function isset_or_int($needle, $haystack) {
        return isset_or($needle, $haystack, 0);
    }
}

if (!function_exists('gen_no_in_pre_date_rnd_32')) {
    function gen_no_in_pre_date_rnd_32($pre = ''): string
    {
        $d = \Carbon\Carbon::now();
        $no = $pre.$d->format('YmdHisu');
        return $no. \Muyuym\Tools\Helpers\Str::random(32 - strlen($no));
    }
}

if (!function_exists('snake_to_camel')) {
    function snake_to_camel($string): string
    {
        return str_replace(' ', '', lcfirst(ucwords(str_replace('_', ' ', strtolower($string)))));
    }
}

if (!function_exists('camel_to_snake')) {
    function camel_to_snake($string): string
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1".'_'."$2", $string));
    }
}

if (!function_exists('date_period')) {
    function date_period($start, $end, string $duration = 'P1D', $format = 'Y-m-d'): array
    {
        $vec = [];
        try {
            $start = new DateTime($start);
            $end = new DateTime($end);

            $period = new DatePeriod($start, new DateInterval($duration), $end);
            foreach ($period as $date) {
                $vec[] = $date->format($format);
            }
        } catch (Throwable $throwable) {
            return $vec;
        }
        return $vec;
    }
}

if (!function_exists('ao_column')) {
    function ao_column($ao, $column_key): array
    {
        $vec = [];
        foreach ($ao as $v) {
            if (is_array($v)) {
                $vec[] = $v[$column_key];
            } elseif (is_object($v)) {
                $vec[] = $v->$column_key;
            }
        }
        return $vec;
    }
}
