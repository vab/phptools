<?php

namespace Muyuym\Tools\X;

class Aes
{
    private string $cipher = 'aes-256-cbc';
    private string $secret_key = '';

    public function __construct(string $secret_key)
    {
        $this->secret_key = $secret_key;
    }

    public function encrypt(string $data): string
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->cipher));
        $encrypted_data = openssl_encrypt($data, $this->cipher, $this->secret_key, OPENSSL_RAW_DATA, $iv);
        return base64_encode($iv . $encrypted_data);
    }

    public function decrypt(string $data): bool|string
    {
        $data = base64_decode($data);
        $iv_length = openssl_cipher_iv_length($this->cipher);
        $iv = substr($data, 0, $iv_length);
        $encrypted_data = substr($data, $iv_length);
        return openssl_decrypt($encrypted_data, $this->cipher, $this->secret_key, OPENSSL_RAW_DATA, $iv);
    }
}
