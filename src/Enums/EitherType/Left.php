<?php

namespace Muyuym\Tools\Enums\EitherType;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;

/**
 * @template L
 * @template R
 *
 * @extends Either<L,R>
 */
final class Left extends Either
{
    /**
     * @var L
     */
    private $value;

    /**
     * @param L $value
     *
     * @return void
     */
    private function __construct($value)
    {
        $this->value = $value;
    }

    public static function create($value)
    {
        return new self($value);
    }

    public function left(): Option
    {
        return Some::create($this->value);
    }

    public function right(): Option
    {
        return None::create();
    }

    /**
     * @param callable(L):L $f
     *
     * @return Either<L,R>
     */
    public function map(callable $f)
    {
        return self::create($f($this->value));
    }

    public function flatMap(callable $f)
    {
        return $f($this->value);
    }
}
